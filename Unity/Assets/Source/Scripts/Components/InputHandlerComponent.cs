﻿//Author: Justine Maurice Orprecio

using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class InputHandler : MonoBehaviour {
	public UnityEvent		 OnClicked;
	//To avoid using an update just to check for a click
	void OnMouseDown() {
		OnClicked.Invoke ();
	}
}