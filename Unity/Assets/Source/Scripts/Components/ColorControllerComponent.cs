﻿//Author: Justine Maurice Orprecio

using UnityEngine;
using System.Collections;

public class ColorControllerComponent : MonoBehaviour {
	//public
	public bool 			RandomizeColorOnStart = true;
	//private
	private Renderer 		rendererCacheRef;
	private Color 			originalColor;

	// Use this for initialization
	void Start () {
		rendererCacheRef = GetComponent<Renderer> ();
		if (RandomizeColorOnStart) {
			/*randomize color*/
			//null check first
			if (rendererCacheRef) {
				//randomize a color
				Color tColor = Color.white;
				//random integer
				int randomColorIndex = Random.Range (0, 4);
				//assign a color to the random int
				switch (randomColorIndex) {
				case 0:
					tColor = Color.red;
					break;
				case 1:
					tColor = Color.blue;
					break;
				case 2:
					tColor = Color.magenta;
					break;
				case 3:
					tColor = Color.yellow;
					break;
				case 4:
					tColor = Color.cyan;
					break;
				}
				rendererCacheRef.material.color = tColor;
			}
		}
		//set original color 
		originalColor =	rendererCacheRef.material.color;
	}

	public void EnableHighlight(){
		//replace with something fancier if I can think of something later on
		if (rendererCacheRef) {
			rendererCacheRef.material.color = Color.green;
		}
	}

	public void DisableHighlight() {
		//replace with something fancier if I can think of something later on
		if (rendererCacheRef) {
			rendererCacheRef.material.color = originalColor;
		}
	}
}