﻿//Author: Justine Maurice Orprecio

using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public enum GameState {
	PlateChoice,
	TowerChoice
}

[System.Serializable]
public class GameRuleConstants{
	public int 				TowerCount;
	public int				PlateCount;
	[System.NonSerialized]
	public GameTower 		GoalTower; 
}

public class GameLogic : MonoBehaviour {
	//public
	public static GameLogic  SingletonInstance = null;
	public GamePlate		 ChosenGamePlate{get;private set;}
	public GameTower		 ChosenGameTower{get;private set;}
	public GameState 		 CurrentGameState{get;private set;}
	public GameRuleConstants GameRules;
	public UnityEvent 		 OnGameWonEvent;
	//private
	//so we know which tower sends a plate
	private GameTower 		  ChosenGamePlateSender;
	private GameBoard		  GameBoardCache;

	void Awake() {
		//Set this as singleton instance as early as possible
		if (!SingletonInstance) {
			SingletonInstance = this;
		} else if (SingletonInstance != this) {
			Destroy (gameObject);
		}
		//Set default start state
		CurrentGameState = GameState.PlateChoice;
	}

	void Start() {
		//let game logic initialize game board to make sure an singleton instance exists before game start
		GameBoardCache = GetComponent<GameBoard>();
		if (GameBoardCache)
			GameBoardCache.InitializeGameBoard (GameRules);
	}

	public void SetGameState(GameState newState) {
		CurrentGameState = newState;
	}

	public void RecognizePlateChoice(GameTower towerSender, GamePlate plate) {
		if (CurrentGameState == GameState.PlateChoice) {
			//set our chosen plate
			ChosenGamePlate = plate;
			//set our sender tower
			ChosenGamePlateSender = towerSender;
			//highlight the plate so we can see our valid choice
			ChosenGamePlate.SendMessage ("EnableHighlight", SendMessageOptions.DontRequireReceiver);
			//pick a tower this time
			SetGameState (GameState.TowerChoice);
		} else
			CancelChoices ();
	}

	public void RecognizeTowerChoice(GameTower chosenTower) {
		//if not yet in tower pick mode, don't recognize pick
		if (CurrentGameState == GameState.TowerChoice) {
			//if tower chosen is not sender and is a neighbor, recognize pick
			if (chosenTower != ChosenGamePlateSender && chosenTower.CanPlaceNewPlate (ChosenGamePlate)) {
				//set our chosen tower
				ChosenGameTower = chosenTower;
				//execute the turn with the chosen plate and tower
				ExecuteTurn ();
			}
		} else
			CancelChoices ();
	}

	public void CancelChoices() {
		//disable chosen entities highlight
		if(ChosenGamePlate)
			ChosenGamePlate.SendMessage("DisableHighlight", SendMessageOptions.DontRequireReceiver);
		//nullify all values
		ChosenGamePlate = null;
		ChosenGameTower = null;
		ChosenGamePlateSender = null;
		//return to plate choice
		SetGameState(GameState.PlateChoice);
	}

	public void ExecuteTurn() {
		//send the chosen plate to the chosen tower
		ChosenGamePlateSender.SendPlate (ChosenGameTower);
		//disable chosen entities highlight
		ChosenGamePlate.SendMessage("DisableHighlight", SendMessageOptions.DontRequireReceiver);

		//Check if the last move won the game
		if (CheckIfHasWon ()) {
			OnGameWonEvent.Invoke();
		} else
		//else, pick a plate again
		SetGameState(GameState.PlateChoice);
	}

	bool CheckIfHasWon() {
		//add more victory conditions here later on
		return  (GameBoardCache.IsGoalTowerFull (GameRules.PlateCount))? true:false;
	}
}