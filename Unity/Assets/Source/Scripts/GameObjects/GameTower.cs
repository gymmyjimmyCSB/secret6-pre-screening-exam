﻿//Author: Justine Maurice Orprecio

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

[System.Serializable]
public class OnPlateValidateByTowerEvent:UnityEvent<GameTower, GamePlate> { }
[System.Serializable]
public class OnTowerClickedEvent:UnityEvent<GameTower> { }

public class GameTower : MonoBehaviour {
	//public
	public	int								TowerHeight;
	public 	int	 							TowerNumber = 1;
	public  OnPlateValidateByTowerEvent		OnPlateValidatedByTower;
	public 	OnTowerClickedEvent				OnTowerClicked;
	//private
	private List<GamePlate> plates = new List<GamePlate>();

	void PlateClickNotify(GamePlate chosenPlate) {
		//only check if plate chosen is top plate then let game logic decide the other factors if pick should be recognized
		bool tPlateChoiceValid = (chosenPlate == plates[plates.Count-1]) ? true : false;
		if (tPlateChoiceValid) {
			//so we can send 2 parameters using send message
			//PlateParcel newParcel = new PlateParcel(this, chosenPlate);
			//notify game logic of pick
			//GameLogic.SingletonInstance.SendMessage ("RecognizePlateChoice", newParcel, SendMessageOptions.DontRequireReceiver);
			OnPlateValidatedByTower.Invoke(this, chosenPlate);
		}
	}

	void OnMouseDown(){
		//Let game logic decide if tower pick should be recognized
		//GameLogic.SingletonInstance.SendMessage ("RecognizeTowerChoice", this);
		OnTowerClicked.Invoke (this);
	}

	public void AddPlate(GamePlate newPlate){
		newPlate.OnClicked.AddListener (PlateClickNotify);
		plates.Add (newPlate);
		newPlate.transform.parent = this.transform;
	}

	public void SendPlate(GameTower receiver) {
		//plate to send will always be the top plate
		GamePlate plateParcel = plates[plates.Count-1];
		//set plate location 
		plateParcel.transform.position = receiver.GetTopspotLocation();
		receiver.AddPlate (plateParcel);
		plateParcel.OnClicked.RemoveListener (PlateClickNotify);
		plates.Remove (plateParcel);
	}

	public Vector3 GetTopspotLocation() {
		if (plates.Count > 0) {
			Bounds bounds = plates[plates.Count - 1].GetComponent<Collider> ().bounds;
			//get upper bound of top plate and add offset so it looks right c:
			return new Vector3 (this.transform.position.x, bounds.max.y+0.2f, this.transform.position.z);
		}
		else {
			Bounds bounds = this.GetComponent<Collider> ().bounds;
			return new Vector3 (this.transform.position.x, bounds.min.y, this.transform.position.z);
		}
	}

	public bool CanPlaceNewPlate(GamePlate other) {
		if (plates.Count > 0) {
			if (other.Size < plates [plates.Count - 1].Size) {
				return true;
			}
			else
				return false;
		}
		return true;
	}

	public int GetPlateCount() {
		return (plates.Count > 0) ? (plates.Count) : 0;
	}
}