﻿//Author: Justine Maurice Orprecio

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameBoard : MonoBehaviour {
	//public
	public GameTower 	TowerPrefab;
	public GamePlate	PlatePrefab;
	public float 		TowerIntervalDist = 3.0f;
	public float 		PlateIntervalDist = 0.5f;
	//private
	private List<GameTower> towers = new List<GameTower> ();
	private GameTower 	GoalTower;

	public void InitializeGameBoard(GameRuleConstants gameRule){
		//first of all, nullcheck.
		if (TowerPrefab) {
			towers.Add (TowerPrefab);
			TowerPrefab.transform.SetParent (this.transform);
			TowerPrefab.TowerHeight = gameRule.PlateCount - 1;
			TowerPrefab.OnPlateValidatedByTower.AddListener (GameLogic.SingletonInstance.RecognizePlateChoice);
			TowerPrefab.OnTowerClicked.AddListener (GameLogic.SingletonInstance.RecognizeTowerChoice);
			GameTower tPrevTower = TowerPrefab;
			for (int i = 1; i < gameRule.TowerCount; i++) {
				Vector3 tNewPos = tPrevTower.transform.position + new Vector3 (TowerIntervalDist, 0, 0);
				GameTower tNewTower = Instantiate (TowerPrefab, tNewPos, this.transform.rotation) as GameTower;
				tNewTower.TowerHeight = gameRule.PlateCount - 1;
				tNewTower.TowerNumber = tPrevTower.TowerNumber + 1;
				tNewTower.OnPlateValidatedByTower.AddListener (GameLogic.SingletonInstance.RecognizePlateChoice);
				tNewTower.OnTowerClicked.AddListener (GameLogic.SingletonInstance.RecognizeTowerChoice);
				tNewTower.transform.SetParent (this.transform);
				tNewTower.transform.localScale = TowerPrefab.transform.localScale;
				towers.Add (tNewTower);
				tPrevTower = tNewTower;
				//if last tower spawned, set it as goal tower
				if(i == gameRule.TowerCount-1)
					GoalTower = tNewTower;
			}
		}
		//first of all, nullcheck.
		if (PlatePrefab) {
			PlatePrefab.transform.SetParent (this.TowerPrefab.transform);
			PlatePrefab.SetSize (gameRule.PlateCount);
			int tInitSize = (int)PlatePrefab.transform.localScale.x;
			Bounds bounds = towers[0].GetComponent<Collider> ().bounds;
			PlatePrefab.transform.position = new Vector3 (towers[0].transform.position.x, bounds.min.y, towers[0].transform.position.z);
			towers[0].AddPlate(PlatePrefab);
			GamePlate tPrevPlate = PlatePrefab;
			for (int i = 1; i < gameRule.PlateCount; i++) {
				//calculate spawn position
				Vector3 tNewPos = tPrevPlate.transform.position + new Vector3 (0, PlateIntervalDist, 0);
				//instantiate new plate
				GamePlate tNewPlate = Instantiate (PlatePrefab, tNewPos, this.transform.rotation) as GamePlate;
				//set owner and size
				towers[0].AddPlate(tNewPlate);
				tNewPlate.transform.localScale = tPrevPlate.transform.localScale;
				tNewPlate.SetSize (tPrevPlate.Size - 1);
				tPrevPlate = tNewPlate;
			}
		}
	}

	public bool IsGoalTowerFull(int maxPlatesInGame) {
		return (GoalTower.GetPlateCount () == maxPlatesInGame) ? true : false;
	}
}
