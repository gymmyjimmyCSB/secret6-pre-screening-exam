﻿//Author: Justine Maurice Orprecio

using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[System.Serializable]
public class OnClickedEvent:UnityEvent<GamePlate> {

}

public class GamePlate : MonoBehaviour {
	//public
	public int 				Size { get; private set; }
	public int 				OrderIndex;
	public OnClickedEvent 	OnClicked;

	public void SetSize(int size){
		Size = size;
		//add an offset to mesh size c:
		transform.localScale = new Vector3 (Size + 1, this.transform.localScale.y, Size + 1);
	}

	void OnMouseDown(){
		//Notify tower owner of click
		OnClicked.Invoke (this);
	}
}