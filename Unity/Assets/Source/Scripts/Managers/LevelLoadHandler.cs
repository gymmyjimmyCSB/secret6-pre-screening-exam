﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelLoadHandler : MonoBehaviour {
	public void ReloadLevel() {
		string tCurrentSceneName = SceneManager.GetActiveScene ().name;
		SceneManager.LoadScene (tCurrentSceneName);
	}

	public void ReloadLevelWithDelay(float delay) {
		StartCoroutine ("DelayReload", delay);
	}

	IEnumerator DelayReload(float delay) {
		yield return new WaitForSeconds (delay);
		ReloadLevel ();
	}
}