﻿using UnityEngine;
using System.Collections;

public class SFXHandler : MonoBehaviour {
	public AudioClip VictorySFX;
	private AudioSource AudioSourceCache;
	// Use this for initialization
	void Start () {
		AudioSourceCache = GetComponent<AudioSource> ();
	}

	public void PlayVictorySFX() {
		AudioSourceCache.PlayOneShot (VictorySFX);
	}
}