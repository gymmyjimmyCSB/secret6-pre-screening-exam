// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "TOHPlayerCharacter.h"
#include "TOHPlayerController.generated.h"

UCLASS()
class TOWEROFHANOI_API ATOHPlayerController : public APlayerController {
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	//setup input
	void SetupInputComponent() override;
	virtual void OnMoveForward(float Value);
	virtual void OnMoveRight(float Value);
	virtual void OnJumpPressed();
	virtual void OnTurnX(float Value);
	virtual void OnTurnY(float Value);
	virtual void OnInteractPressed();
	//	Protected variables
	ATOHPlayerCharacter* CastedPawn;
public:
	//constructor
	ATOHPlayerController();
};
