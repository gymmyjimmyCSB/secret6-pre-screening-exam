// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TOHInterfaceInteractable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTOHInterfaceInteractable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class TOWEROFHANOI_API ITOHInterfaceInteractable
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact")
		void Interact();
};
