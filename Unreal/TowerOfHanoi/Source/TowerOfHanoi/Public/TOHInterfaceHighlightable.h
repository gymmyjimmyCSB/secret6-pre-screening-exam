// Author: Justine Orprecio

#pragma once

#include "TOHInterfaceHighlightable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTOHInterfaceHighlightable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class TOWEROFHANOI_API ITOHInterfaceHighlightable
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Highlight")
		void Highlight();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Highlight")
		void RemoveHighlight();
};
