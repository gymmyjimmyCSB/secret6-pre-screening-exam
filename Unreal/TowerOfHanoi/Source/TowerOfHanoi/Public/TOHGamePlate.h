//Author: Justine Maurice Orprecio

#pragma once

#include "GameFramework/Actor.h"
#include "TOHInterfaceInteractable.h"
#include "TOHInterfaceHighlightable.h"
#include "TOHGamePlate.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPlateInteractEvent, ATOHGamePlate*, InteractedPlate);

UCLASS()
class TOWEROFHANOI_API ATOHGamePlate : public AActor, public ITOHInterfaceInteractable, public ITOHInterfaceHighlightable
{
	GENERATED_BODY()
	
public:	
	//	Sets default values for this actor's properties
	ATOHGamePlate();
	int			 GetSize() { return Size; }
	void		 SetSize(int NewSize);
	UPROPERTY(BlueprintAssignable, Category = "GameplayEvent")
		FPlateInteractEvent OnPlateInteracted;
	//	Interact Interface 
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
		void Interact();
protected:
	//	Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	int			 Size;
	//	Interface Implementation
	virtual void Interact_Implementation() override;
};