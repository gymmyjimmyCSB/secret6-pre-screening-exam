// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "TOHInterfaceInteractable.h"
#include "TOHPlayerCharacter.generated.h"

//Forward declaration
class UCharacterState;

USTRUCT()
struct FInteractableCheckData {
	GENERATED_USTRUCT_BODY()
	FVector						TraceStart;
	FVector						TraceEnd;
	FVector						HitLocation;
	float						Distance;
	AActor*						HitActor;
	bool						bIsInteractable;
	FInteractableCheckData() {
		HitActor = NULL;
		bIsInteractable = false;
	}
	~FInteractableCheckData() {}
};

UCLASS()
class TOWEROFHANOI_API ATOHPlayerCharacter : public ACharacter {
	GENERATED_BODY()
public:
	// Sets default values for this pawn's properties
	ATOHPlayerCharacter();
	//	Pawn's game state
	UCharacterState* CurrentState;
	//	Set pawn game state
	template<class T> void SetCharacterState();
	//	Called by player controller to let pawn state decide what to do
	void InterpretKey(FKey input);
	//	Movement functions called by pawn state
	void MoveForward(float Value);
	void MoveRight(float Value);
	void Jump();
	void Interact();
	void CheckForInteractable(FInteractableCheckData &CheckData);
	//	Public var
	UPROPERTY(EditAnywhere, Category = Character)
		float MaxInteractionDist = 500.f;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
};

//*State Machine for pawn--------------------------*///
UCLASS(Blueprintable)
class TOWEROFHANOI_API UCharacterState : public UObject {
	GENERATED_BODY()
public:
	virtual ~UCharacterState() {}
	virtual void HandleInput(ATOHPlayerCharacter* pawn, FKey input) {}
	virtual void Update(ATOHPlayerCharacter* pawn, float deltaTime) {}
	virtual void OnEnter(ATOHPlayerCharacter* pawn) {}
	virtual void OnExit(ATOHPlayerCharacter* pawn) {}
};

UCLASS(Blueprintable)
class TOWEROFHANOI_API UIdleState : public UCharacterState {
	GENERATED_BODY()
public:
	virtual ~UIdleState() {}
	virtual void HandleInput(ATOHPlayerCharacter* pawn, FKey input);
	virtual void Update(ATOHPlayerCharacter* pawn, float deltaTime) {}
	virtual void OnEnter(ATOHPlayerCharacter* pawn) {}
	virtual void OnExit(ATOHPlayerCharacter* pawn) {}
};

UCLASS(Blueprintable)
class TOWEROFHANOI_API UWalkState : public UIdleState {
	GENERATED_BODY()
public:
	virtual ~UWalkState() {}
	virtual void HandleInput(ATOHPlayerCharacter* pawn, FKey input);
	virtual void Update(ATOHPlayerCharacter* pawn, float deltaTime) {}
	virtual void OnEnter(ATOHPlayerCharacter* pawn) {}
	virtual void OnExit(ATOHPlayerCharacter* pawn) {}
};

UCLASS(Blueprintable)
class TOWEROFHANOI_API UJumpState : public UIdleState {
	GENERATED_BODY()
public:
	virtual ~UJumpState() {}
	virtual void HandleInput(ATOHPlayerCharacter* pawn, FKey input);
	virtual void Update(ATOHPlayerCharacter* pawn, float deltaTime);
	virtual void OnEnter(ATOHPlayerCharacter* pawn);
	virtual void OnExit(ATOHPlayerCharacter* pawn) {}
};