//Author: Justine Maurice Orprecio

#pragma once

#include "GameFramework/Actor.h"
#include "TOHGamePlate.h"
#include "TOHInterfaceInteractable.h"
#include "TOHInterfaceHighlightable.h"
#include "TOHGameTower.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPlateValidNotifyEvent, ATOHGamePlate*, InteractedPlate, ATOHGameTower*, TowerNotifier);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTowerInteractEvent, ATOHGameTower*, InteractedTower);

UCLASS()
class TOWEROFHANOI_API ATOHGameTower : public AActor, public ITOHInterfaceInteractable, public ITOHInterfaceHighlightable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATOHGameTower();
	void		AddPlate(ATOHGamePlate* NewPlate);
	void		RemovePlate(ATOHGamePlate* NewPlate);
	void		SendPlate(ATOHGameTower* TowerReceiver);
	FVector		GetTopspotLocation();
	bool		CanPlaceNewPlate(ATOHGamePlate* OtherPlate);
	int			GetPlateCount() { return Plates.Num(); }
	int			GetHeight() { return TowerHeight; }
	void		SetHeight(int Height);
	int			GetNumber() { return TowerNumber; }
	void		SetNumber(int Number);
	float		PlateDistance = 100.f;
	// Events
	UPROPERTY(BlueprintAssignable, Category = "GameplayEvent")
		FPlateValidNotifyEvent OnPlateValidatedByTower;
	UPROPERTY(BlueprintAssignable, Category = "GameplayEvent")
		FTowerInteractEvent OnTowerInteract;
	//	Interact Interface
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
		void Interact();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	// Let tower validate it's own conditions on plate pick first before sending to Game Mode logic
	UFUNCTION()
	void		ValidatePlatePick(ATOHGamePlate* Plate);
	int			TowerHeight;
	int			TowerNumber;
	TArray<ATOHGamePlate*> Plates;
	//	Interface Implementation
	virtual void Interact_Implementation() override;
};