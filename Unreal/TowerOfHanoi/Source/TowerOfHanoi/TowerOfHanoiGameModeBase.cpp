//Author: Justine Maurice Orprecio

#include "TowerOfHanoi.h"
#include "TowerOfHanoiGameModeBase.h"

void ATowerOfHanoiGameModeBase::BeginPlay() {
	if (GameTowerClass->IsValidLowLevel() && GamePlateClass->IsValidLowLevel()) {
		// Instantiate towers and plates
		InitiateGameBoard();
		// Let players start by picking plate first
		SetGameTurn(EGameTurn::PlateChoice);
		// Set game as "unfinished". Also check if player somehow already won/lost lol
		IsGoalComplete();
	}
	else
		GEngine->AddOnScreenDebugMessage(-1, 100.f, FColor::Red, TEXT("Error: Missing Class Defaults in Game Mode."));
}

void ATowerOfHanoiGameModeBase::InitiateGameBoard() {
	//Spawn the towers
	//First spawn for reference
	FActorSpawnParameters _TowerSpawnParam;
	_TowerSpawnParam.Owner = this;
	ATOHGameTower* _InitialTower = GetWorld()->SpawnActor<ATOHGameTower>(GameTowerClass, _TowerSpawnParam);
	_InitialTower->SetHeight(PlateCount - 1);
	_InitialTower->SetNumber(1);
	_InitialTower->PlateDistance = PlateSpawnDistance;
	_InitialTower->OnPlateValidatedByTower.AddDynamic(this, &ATowerOfHanoiGameModeBase::PlatePickValidate);
	_InitialTower->OnTowerInteract.AddDynamic(this, &ATowerOfHanoiGameModeBase::TowerPickValidate);
	GameTowers.Add(_InitialTower);
	ATOHGameTower* _PreviousTower = _InitialTower;
	//Loop the rest
	for (int i = 1; i < TowerCount; i++) {
		ATOHGameTower* _NewTower = GetWorld()->SpawnActor<ATOHGameTower>(GameTowerClass, _TowerSpawnParam);
		FVector _NewTowerLocation = _PreviousTower->GetActorLocation() + FVector(TowerSpawnDistance, 0, 0);
		_NewTower->SetActorLocation(_NewTowerLocation);
		_NewTower->SetHeight(PlateCount - 1);
		_NewTower->SetNumber(_PreviousTower->GetNumber() + 1);
		_NewTower->PlateDistance = PlateSpawnDistance;
		_NewTower->OnPlateValidatedByTower.AddDynamic(this, &ATowerOfHanoiGameModeBase::PlatePickValidate);
		_NewTower->OnTowerInteract.AddDynamic(this, &ATowerOfHanoiGameModeBase::TowerPickValidate);
		GameTowers.Add(_NewTower);
		_PreviousTower = _NewTower;
		if (i == TowerCount-1)
			GoalTower = _NewTower;
	}

	//Spawn the plates
	bool _bIsInitialTowerValid = (_InitialTower->IsValidLowLevel()) ? true : false;
	if (_bIsInitialTowerValid) {
		//ATOHGameTower* _InitialTower = GameTowers[0];
		FActorSpawnParameters _PlateSpawnParam;
		_PlateSpawnParam.Owner = this;
		ATOHGamePlate* _InitialPlate = GetWorld()->SpawnActor<ATOHGamePlate>(GamePlateClass, _PlateSpawnParam);
		_InitialPlate->SetActorLocation(_InitialTower->GetActorLocation());
		_InitialPlate->SetSize(PlateCount);
		_InitialTower->AddPlate(_InitialPlate);
		ATOHGamePlate* _PreviousPlate = _InitialPlate;
		//Loop the rest
		for (int i = 1; i < PlateCount; i++) {
			ATOHGamePlate* _NewPlate = GetWorld()->SpawnActor<ATOHGamePlate>(GamePlateClass, _PlateSpawnParam);
			FVector _NewPlateLocation = _PreviousPlate->GetActorLocation() + FVector(0 ,0, PlateSpawnDistance);
			_NewPlate->SetActorLocation(_NewPlateLocation);
			_NewPlate->SetSize(_PreviousPlate->GetSize() - 1);
			FVector _NewPlateMeshScale = FVector(_PreviousPlate->GetActorScale3D().X - PlateMeshShrinkSize,
				_PreviousPlate->GetActorScale3D().Y - PlateMeshShrinkSize,
				_PreviousPlate->GetActorScale3D().Z);
			_NewPlate->SetActorScale3D(_NewPlateMeshScale);
			GameTowers[0]->AddPlate(_NewPlate);
			_PreviousPlate = _NewPlate;
		}
	}
}

void ATowerOfHanoiGameModeBase::PlatePickValidate(ATOHGamePlate* Plate, ATOHGameTower* TowerNotifier) {
	if (CurrentGameTurn == EGameTurn::PlateChoice) {
		if (Plate->IsValidLowLevel() && !IsGoalComplete()) {
			ChosenPlate = Plate;
			ChosenPlateTowerSender = TowerNotifier;
			SetGameTurn(EGameTurn::TowerChoice);
			OnPlatePickValidated.Broadcast();
		}
	}
	else {
		if (Plate->IsValidLowLevel() && !IsGoalComplete()) {
			SetGameTurn(EGameTurn::PlateChoice);
			OnChoicesCancelled.Broadcast();
			ChosenPlate = Plate;
			ChosenPlateTowerSender = TowerNotifier;
			SetGameTurn(EGameTurn::TowerChoice);
			OnPlatePickValidated.Broadcast();
		}
	}
}

void ATowerOfHanoiGameModeBase::TowerPickValidate(ATOHGameTower* Tower) {
	if (CurrentGameTurn == EGameTurn::TowerChoice && Tower->IsValidLowLevel() && !IsGoalComplete()) {
		bool _bAdditionalValidation = (ChosenPlateTowerSender != Tower && Tower->CanPlaceNewPlate(ChosenPlate)) ? true : false;
		if (_bAdditionalValidation) {
			ChosenTower = Tower;
			OnTowerPickValidated.Broadcast();
			ExecuteTurn();
		}
	}
}

void ATowerOfHanoiGameModeBase::ResetChoices() {
	ChosenPlate = NULL;
	ChosenTower = NULL;
	ChosenPlateTowerSender = NULL;
}

void ATowerOfHanoiGameModeBase::ExecuteTurn() {
	bool _bAreChoicesValid = (ChosenPlate->IsValidLowLevel() && ChosenTower->IsValidLowLevel() && ChosenPlateTowerSender->IsValidLowLevel())? true:false;
	if (_bAreChoicesValid) {
		ChosenPlateTowerSender->SendPlate(ChosenTower);
		//	Call event first before clearing chosen plate/tower references
		OnTurnExecuted.Broadcast();
		ResetChoices();
		if (!IsGoalComplete()) {
			SetGameTurn(EGameTurn::PlateChoice);
		}
	}
	else {
		ResetChoices();
		SetGameTurn(EGameTurn::PlateChoice);
		OnTurnExecuted.Broadcast();
		GEngine->AddOnScreenDebugMessage(-1, 100.f, FColor::Red, TEXT("Error: Game choices invalid. Please repick."));
	}
}

bool ATowerOfHanoiGameModeBase::IsGoalComplete() {
	if (GoalTower->GetPlateCount() == PlateCount) {
		CurrentGameCondition = EGameCondition::Win;
		FinishGame();
		return true;
	}
	CurrentGameCondition = EGameCondition::Unfinished;
	return false;
}

void ATowerOfHanoiGameModeBase::SetGameTurn(EGameTurn NewTurn) {
	CurrentGameTurn = NewTurn;
}

void ATowerOfHanoiGameModeBase::FinishGame() {
	OnGameHasFinished.Broadcast();
}