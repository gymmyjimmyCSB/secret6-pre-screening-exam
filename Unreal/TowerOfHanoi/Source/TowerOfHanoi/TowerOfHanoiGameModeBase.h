//Author: Justine Maurice Orprecio

#pragma once

#include "GameFramework/GameModeBase.h"
#include "TOHGameTower.h"
#include "TOHGamePlate.h"
#include "TowerOfHanoiGameModeBase.generated.h"

enum EGameCondition {
	Unfinished,
	Win,
	Lose
};

enum EGameTurn {
	PlateChoice,
	TowerChoice
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTurnExecutedEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGameFinishedEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTowerPickValidatedEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlatePickValidatedEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FChoicesCancelledEvent);

UCLASS()
class TOWEROFHANOI_API ATowerOfHanoiGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	//	Tower references for game logic
	TArray<ATOHGameTower*> GameTowers;
	// Tower to fill to win the game
	ATOHGameTower* GoalTower;
	// Is the game unfinished, won or lost?
	EGameCondition CurrentGameCondition;
	// Is it time to pick a plate or tower
	EGameTurn	   CurrentGameTurn;
	// validated chosen game plate
	ATOHGamePlate* ChosenPlate;
	// tower that sent the game plate
	ATOHGameTower* ChosenPlateTowerSender;
	// validated chosen game tower
	ATOHGameTower* ChosenTower;

	virtual void BeginPlay() override;
	//	Check if the plate the player chose is valid
	UFUNCTION() 
		void PlatePickValidate(ATOHGamePlate* Plate, ATOHGameTower* TowerNotifier);
	//	Check if the tower the player chose is valid
	UFUNCTION() 
		void TowerPickValidate(ATOHGameTower* Tower);
	//	Instantiate game towers and plates
	void InitiateGameBoard();
	//	nullify choices
	void ResetChoices();
	//	If player has chosen a plate and tower, send the chosen plate to the chosen tower
	void ExecuteTurn();
	//	game has finished
	void FinishGame();
public:
	UPROPERTY(EditAnywhere, Category = "GameBoardSetup")
		int TowerCount = 3;
	UPROPERTY(EditAnywhere, Category = "GameBoardSetup")
		int PlateCount = 3;
	//	How far apart the towers will be spawned.
	UPROPERTY(EditAnywhere, Category = "GameBoardSetup")
		int TowerSpawnDistance = 3;
	//	How far apar the plates will be spawned
	UPROPERTY(EditAnywhere, Category = "GameBoardSetup")
		int PlateSpawnDistance = 3;
	UPROPERTY(EditAnywhere, Category = "GameBoardSetup")
		float PlateMeshShrinkSize = 0.2f;
	//	What game tower class to spawn
	UPROPERTY(EditAnywhere, Category = "Classes")
		class TSubclassOf<AActor> GameTowerClass;
	//	What game plate class to spawn
	UPROPERTY(EditAnywhere, Category = "Classes")
		class TSubclassOf<AActor> GamePlateClass;
	//	When goal tower is complete
	UPROPERTY(BlueprintAssignable, Category = "GameplayEvent")
		FGameFinishedEvent OnGameHasFinished;
	UPROPERTY(BlueprintAssignable, Category = "GameplayEvent")
		FTurnExecutedEvent OnTurnExecuted;
	UPROPERTY(BlueprintAssignable, Category = "GameplayEvent")
		FPlatePickValidatedEvent OnPlatePickValidated;
	UPROPERTY(BlueprintAssignable, Category = "GameplayEvent")
		FTowerPickValidatedEvent OnTowerPickValidated;
	UPROPERTY(BlueprintAssignable, Category = "GameplayEvent")
		FChoicesCancelledEvent OnChoicesCancelled;
	//	Check and set if game is still in progress, already won or lost
	bool IsGoalComplete();
	//	Whether it is time to pick a plate or tower
	void SetGameTurn(EGameTurn NewTurn);
	UFUNCTION(BlueprintCallable, Category="Gameplay")
		ATOHGameTower* GetChosenTower() { return ChosenTower; }
	UFUNCTION(BlueprintCallable, Category = "Gameplay")
		ATOHGamePlate* GetChosenPlate() { return ChosenPlate; }
};
