// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerOfHanoi.h"
#include "TOHInterfaceInteractable.h"


// This function does not need to be modified.
UTOHInterfaceInteractable::UTOHInterfaceInteractable(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}