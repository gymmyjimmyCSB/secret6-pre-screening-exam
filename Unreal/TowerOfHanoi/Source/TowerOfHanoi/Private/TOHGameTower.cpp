//Author: Justine Maurice Orprecio

#include "TowerOfHanoi.h"
#include "TOHGameTower.h"

// Sets default values
ATOHGameTower::ATOHGameTower() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATOHGameTower::BeginPlay() {
	Super::BeginPlay();
}

// Called every frame
void ATOHGameTower::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

void ATOHGameTower::AddPlate(ATOHGamePlate* NewPlate) {
	NewPlate->SetOwner(this);
	//	Bind new plate's interact event to this tower for validation when picked
	NewPlate->OnPlateInteracted.AddDynamic(this, &ATOHGameTower::ValidatePlatePick);
	Plates.Add(NewPlate);
}

void ATOHGameTower::RemovePlate(ATOHGamePlate* NewPlate) {
	//	Unbind plate interact event
	NewPlate->OnPlateInteracted.RemoveDynamic(this, &ATOHGameTower::ValidatePlatePick);
	Plates.Remove(NewPlate);
}

void ATOHGameTower::SendPlate(ATOHGameTower* TowerReceiver) {
	if (GetPlateCount() > 0) {
		ATOHGamePlate* _PlateParcel = Plates[Plates.Num() - 1];
		FVector _NewLocation = TowerReceiver->GetTopspotLocation();
		_PlateParcel->SetActorLocation(_NewLocation);
		//	Send to to other tower
		RemovePlate(_PlateParcel);
		TowerReceiver->AddPlate(_PlateParcel);
	}
}

FVector ATOHGameTower::GetTopspotLocation() {
	FVector _TopspotLocation = GetActorLocation();
	if (GetPlateCount() > 0) 
		_TopspotLocation = FVector(GetActorLocation().X, GetActorLocation().Y, Plates[Plates.Num() - 1]->GetActorLocation().Z + PlateDistance);
	return _TopspotLocation;
}

bool ATOHGameTower::CanPlaceNewPlate(ATOHGamePlate* OtherPlate) {
	if (GetPlateCount() > 0 && Plates[Plates.Num() - 1]->IsValidLowLevel()) {
		if (OtherPlate->GetSize() > Plates[Plates.Num() - 1]->GetSize())
			return false;
	}
	return true;
}

void ATOHGameTower::SetHeight(int Height) {
	TowerHeight = Height;
}

void ATOHGameTower::SetNumber(int Number) {
	TowerNumber = Number;
}

void ATOHGameTower::ValidatePlatePick(ATOHGamePlate* Plate) {
	if(Plate == Plates[Plates.Num()-1])
		OnPlateValidatedByTower.Broadcast(Plate, this);
}

void ATOHGameTower::Interact_Implementation() {
	OnTowerInteract.Broadcast(this);
}