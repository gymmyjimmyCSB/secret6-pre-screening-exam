// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerOfHanoi.h"
#include "TOHPlayerController.h"



ATOHPlayerController::ATOHPlayerController() {
	PrimaryActorTick.bCanEverTick = true;
}

void ATOHPlayerController::BeginPlay() {
	CastedPawn = Cast<ATOHPlayerCharacter>(GetPawn());
}

void ATOHPlayerController::SetupInputComponent() {
	Super::SetupInputComponent();
	InputComponent->BindAxis("MoveForward", this, &ATOHPlayerController::OnMoveForward);
	InputComponent->BindAxis("MoveRight", this, &ATOHPlayerController::OnMoveRight);
	InputComponent->BindAxis("TurnX", this, &ATOHPlayerController::OnTurnX);
	InputComponent->BindAxis("TurnY", this, &ATOHPlayerController::OnTurnY);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ATOHPlayerController::OnJumpPressed);
	InputComponent->BindAction("Interact", IE_Pressed, this, &ATOHPlayerController::OnInteractPressed);
}

void ATOHPlayerController::OnMoveForward(float Val) {
	if (Val > 0)
	CastedPawn->InterpretKey(EKeys::W);
	else if (Val < 0)
	CastedPawn->InterpretKey(EKeys::S);
}

void ATOHPlayerController::OnTurnX(float Val) {
	GetPawn()->AddControllerYawInput(Val);
}

void ATOHPlayerController::OnTurnY(float Val) {
	GetPawn()->AddControllerPitchInput(Val);
}

void ATOHPlayerController::OnMoveRight(float Val) {
	if (Val > 0)
	CastedPawn->InterpretKey(EKeys::D);
	else if (Val < 0)
	CastedPawn->InterpretKey(EKeys::A);
}

void ATOHPlayerController::OnJumpPressed() {
	CastedPawn->InterpretKey(EKeys::SpaceBar);
}

void ATOHPlayerController::OnInteractPressed() {
	CastedPawn->InterpretKey(EKeys::LeftMouseButton);
}
