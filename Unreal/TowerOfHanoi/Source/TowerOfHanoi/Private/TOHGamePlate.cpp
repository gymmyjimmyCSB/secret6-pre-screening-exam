//Author: Justine Maurice Orprecio

#include "TowerOfHanoi.h"
#include "TOHGamePlate.h"


// Sets default values
ATOHGamePlate::ATOHGamePlate() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATOHGamePlate::BeginPlay() {
	Super::BeginPlay();
	
}

// Called every frame
void ATOHGamePlate::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

void ATOHGamePlate::SetSize(int NewSize) {
	Size = NewSize;
}

void ATOHGamePlate::Interact_Implementation() {
	OnPlateInteracted.Broadcast(this);
}