// Author: Justine Orprecio

#include "TowerOfHanoi.h"
#include "TOHInterfaceHighlightable.h"


// This function does not need to be modified.
UTOHInterfaceHighlightable::UTOHInterfaceHighlightable(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any ITOHInterfaceHighlightable functions that are not pure virtual.
