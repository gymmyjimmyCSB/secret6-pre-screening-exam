// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerOfHanoi.h"
#include "TOHPlayerCharacter.h"

// Sets default values
ATOHPlayerCharacter::ATOHPlayerCharacter() {
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//	Let's see if we can make this thing work without using Tick.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATOHPlayerCharacter::BeginPlay() {
	Super::BeginPlay();
	//Initialize State
	SetCharacterState<UIdleState>();
}

void ATOHPlayerCharacter::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	if (CurrentState)
		CurrentState->Update(this, DeltaTime);
}

void ATOHPlayerCharacter::MoveForward(float Value) {
	FRotator Rotation = Controller->GetControlRotation();
	const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
	// add movement in that direction
	AddMovementInput(Direction, Value);
}

void ATOHPlayerCharacter::MoveRight(float Value) {
	const FRotator Rotation = Controller->GetControlRotation();
	const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);
	// add movement in that direction
	AddMovementInput(Direction, Value);
}

void ATOHPlayerCharacter::Jump() {
	ACharacter::Jump();
}

void ATOHPlayerCharacter::CheckForInteractable(FInteractableCheckData &CheckData) {
	FHitResult HitData(ForceInit);
	FCollisionQueryParams TraceParams(FName(TEXT("InteractTrace")), true, this);
	TraceParams.AddIgnoredActor(this);
	GetWorld()->LineTraceSingleByChannel(
		HitData,				//	Trace Hit Data
		CheckData.TraceStart,	//	Trace Start
		CheckData.TraceEnd,		//	Trace End
		ECC_Visibility,			//	Trace Channel
		TraceParams				//	Trace Parameters
	);

	if (HitData.bBlockingHit) {
		if(HitData.Actor.IsValid())
			CheckData.HitActor = HitData.Actor.Get();
		ITOHInterfaceInteractable* _InteractableInterface = Cast<ITOHInterfaceInteractable>(HitData.Actor.Get());
		bool _bIsHitActorInteractable = (_InteractableInterface)? true:false;
		CheckData.bIsInteractable = _bIsHitActorInteractable;
	}
}

void ATOHPlayerCharacter::Interact() {
	//	Compute center of screen for trace
	FVector CamLoc;
	FRotator CamRot;
	GetController()->GetPlayerViewPoint(CamLoc, CamRot);
	FVector FinalAim = CamRot.Vector();
	FVector OutStartTrace = FVector::ZeroVector;
	FRotator DummyRot;
	GetController()->GetPlayerViewPoint(OutStartTrace, DummyRot);
	FVector CameraPos = OutStartTrace = OutStartTrace + FinalAim * (FVector::DotProduct((Instigator->GetActorLocation() - OutStartTrace), FinalAim));
	const FVector EndPos = CameraPos + (FinalAim * MaxInteractionDist);

	FInteractableCheckData _InteractableCheckData;
	_InteractableCheckData.TraceStart = CameraPos;
	_InteractableCheckData.TraceEnd = EndPos;
	CheckForInteractable(_InteractableCheckData);
	if (_InteractableCheckData.bIsInteractable) {
		ITOHInterfaceInteractable* _InteractableInterface = Cast<ITOHInterfaceInteractable>(_InteractableCheckData.HitActor);
		_InteractableInterface->Execute_Interact(_InteractableCheckData.HitActor);
	}
}

template<class T>
void ATOHPlayerCharacter::SetCharacterState() {
	UCharacterState* _Holder = NewObject<T>();//  new T();
	//does current state already exist?
	if (CurrentState != NULL) {
		if (_Holder) {
			//Because garbage collector will eventually eat this object if we don't staple it to root
			_Holder->AddToRoot();
			//Call any exit state functionality
			CurrentState->OnExit(this);
			//Set new state and remove old state from root to let garbage collector do its job
			UCharacterState* _t = CurrentState;
			CurrentState = _Holder;
			_t->RemoveFromRoot();
			//call current state enter functionality
			CurrentState->OnEnter(this);
		}
	}
	//if not, just set the desired state and attach it to root
	else {
		CurrentState = _Holder;
		//call current state enter functionality
		CurrentState->OnEnter(this);
		_Holder->AddToRoot();
	}
}

void ATOHPlayerCharacter::InterpretKey(FKey key) {

	CurrentState->HandleInput(this, key);
}

//Character HSM
void UIdleState::HandleInput(ATOHPlayerCharacter* pawn, FKey input) {
	if (input == EKeys::LeftMouseButton) {
		pawn->Interact();
	}

	if (input == EKeys::W || input == EKeys::S ||
		input == EKeys::A || input == EKeys::D) {
		pawn->SetCharacterState<UWalkState>();
	}

	else if (input == EKeys::SpaceBar) {
		pawn->SetCharacterState<UJumpState>();
	}
}

void UWalkState::HandleInput(ATOHPlayerCharacter* pawn, FKey input) {
	if (input == EKeys::LeftMouseButton) {
		pawn->Interact();
	}

	if (input == EKeys::W)
		pawn->MoveForward(1.0f);
	else if (input == EKeys::S)
		pawn->MoveForward(-1.0f);
	else if (input == EKeys::D)
		pawn->MoveRight(1.0f);
	else if (input == EKeys::A)
		pawn->MoveRight(-1.0f);
	else
		UIdleState::HandleInput(pawn, input);
}

void UJumpState::OnEnter(ATOHPlayerCharacter* Character) {
	Character->Jump();
}

void UJumpState::HandleInput(ATOHPlayerCharacter* pawn, FKey input) {
	if (input == EKeys::LeftMouseButton) {
		pawn->Interact();
	}
}

void UJumpState::Update(ATOHPlayerCharacter* Character, float DeltaTime) {
	if (!Character->GetCharacterMovement()->IsFalling()) {
		Character->SetCharacterState<UIdleState>();
	}
}